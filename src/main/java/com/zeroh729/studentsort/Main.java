package com.zeroh729.studentsort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class Main {
    private static ArrayList<Student> students;
    
    public static void main(String[] args) {
       students = new ArrayList();
       students = DatabaseConnection.fetchData();
       
       printClassList(students);
       
       printSortedList(students, StudentSorter.ByLastName);
       printSortedList(students, StudentSorter.ByLastNameDesc);
       printSortedList(students, StudentSorter.ByAge);
       printSortedList(students, StudentSorter.ByAgeDesc);
       printSortedList(students, StudentSorter.ByGender);
       printSortedList(students, StudentSorter.ByGenderDesc);
       printSortedList(students, StudentSorter.ByGenderAndLastName);
    }
    
    private static void printClassList(ArrayList<Student> students){
       System.out.println("\n-------------------------------------------------------------------------------------------------");
       System.out.println("ALGORTM 101 (ALdub GalORe Therapeutic Marathon class 101) - OFFICIAL ENROLLED STUDENT CLASS LIST"); 
       System.out.println("-------------------------------------------------------------------------------------------------");
       for(Student student : students){
           System.out.println(student.toString());
       }
    }
    
    private static void printSortedList(ArrayList<Student> students, Comparator comparator){
       ArrayList<Student> tempStuds = (ArrayList<Student>) students.clone();

       System.out.println("\n-------------------------");
       Collections.sort(tempStuds, comparator);
       System.out.println(comparator.toString());
       
       System.out.println("-------------------------");
       for(Student student : tempStuds){
                System.out.println(student);
       }
    }
    
}
