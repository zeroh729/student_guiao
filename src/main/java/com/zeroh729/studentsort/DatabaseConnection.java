package com.zeroh729.studentsort;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class DatabaseConnection {
    public static ArrayList<Student> fetchData(){
        ArrayList<Student> students = new ArrayList<>();
        students.add(new Student("Taylor", "Swift", "Harris", 'm', "10/02/1989" , "Hollywood Street, California"));
        students.add(new Student("Nadine", "Lustre", "Padilla", 'f', "10/12/1975" , "GMA Kamuning, Quezon City"));
        students.add(new Student("Lynda", "Dot", "Com", 'f', "08/01/1976" , "Nashville, USA"));
        students.add(new Student("Rodrigo", "Duterte", "Cayetano", 'f', "02/14/1999" , "White House, Davao"));
        students.add(new Student("Kris", "Girl", "Abunda", 'f', "02/13/1994" , "ABS-CBN Compound"));
        students.add(new Student("Jake", "Will", "Knapp", 'm', "05/13/2001" , "Florida, USA"));
        students.add(new Student("Jejomar", "Binay", "Cayetano", 'f', "01/31/2004" , "Ayala Ave. Makati"));
        students.add(new Student("Mar", "Roxas", "Cayetano III", 'm', "03/02/1983" , "Ninoy Aquino International Airport 4"));
        students.add(new Student("Poe", "Edgar Alan", "The Cayetano III", 'm', "02/28/1988" , "Boston, Massachusetts, USA"));
        students.add(new Student("Hayao", "Miyazaki", "Rogers", 'f', "11/04/1989" , "Tokyo, Japan"));
        return students;
    }
}
