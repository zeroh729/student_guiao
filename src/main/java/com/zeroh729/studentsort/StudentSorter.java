package com.zeroh729.studentsort;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

public class StudentSorter{
    
    public static Comparator<Student> ByLastName = new Comparator<Student>() {
        @Override
        public int compare(Student s1, Student s2) {
               return s1.getLastName().compareTo(s2.getLastName());
        }

        @Override
        public String toString() {
            return "Sorted by Last Name (Asc)";
        }
    };
    
    public static Comparator<Student> ByLastNameDesc = new Comparator<Student>() {
        @Override
        public int compare(Student s1, Student s2) {
               return s2.getLastName().compareTo(s1.getLastName());
        }
        
        @Override
        public String toString() {
            return "Sorted by Last Name (Desc)";
        }
    };
    
    public static Comparator ByGender = new Comparator<Student>(){
        @Override
        public int compare(Student s1, Student s2) {
            return s1.getGender() == 'f' ? 0 : -1;
        }
        
        @Override
        public String toString() {
            return "Sorted by Gender (Asc)";
        }
    };
    
    public static Comparator ByGenderDesc = new Comparator<Student>(){
        @Override
        public int compare(Student s1, Student s2) {            
            return s1.getGender() == 'm' ? 0 : -1;
        }
        
        @Override
        public String toString() {
            return "Sorted by Gender (Desc)";
        }
    };
    
    public static Comparator ByAge = new Comparator<Student>(){
        @Override
        public int compare(Student s1, Student s2) {
            return s1.getAge() > s2.getAge() ? 0 : -1;
        }
        
        @Override
        public String toString() {
            return "Sorted by Age (Asc)";
        }
    };
    
    public static Comparator ByAgeDesc = new Comparator<Student>(){
        @Override
        public int compare(Student s1, Student s2) {
            return s1.getAge() <= s2.getAge() ? 0 : -1;
        }
        
        @Override
        public String toString() {
            return "Sorted by Age (Desc)";
        }
    };
    
    public static Comparator ByGenderAndLastName = new Comparator<Student>(){
        @Override
        public int compare(Student s1, Student s2) {
            if(s1.getGender() == s2.getGender()){
                int x = s1.getLastName().compareTo(s2.getLastName());
                return x < 0 ? -1 : 0;
            }else{
                return s1.getGender() == 'm' ? -1 : 0;
            }
        }

        @Override
        public String toString() {
            return "Sorted by Gender and Last Name";
        }
    };
}
