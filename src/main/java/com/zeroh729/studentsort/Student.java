package com.zeroh729.studentsort;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import org.joda.time.DateTime;
import org.joda.time.LocalDate;
import org.joda.time.Period;
import org.joda.time.PeriodType;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;

public class Student {
    private String firstName;
    private String middleName;
    private String lastName;
    private int age;
    private char gender;
    private DateTime birthdate;
    private String address;

    public Student(String firstName, String middleName, String lastName, char gender, String birthdate, String address) {
        DateTimeFormatter format = DateTimeFormat.forPattern("M/dd/yyyy");
        DateTime birthdatetime = format.parseDateTime(birthdate);
        
        setData(firstName, middleName, lastName, gender, birthdatetime, address);
    }
    
    public Student(String firstName, String middleName, String lastName, char gender, DateTime birthdate, String address) {
        setData(firstName, middleName, lastName, gender, birthdate, address);
    }
    
    private void setData(String firstName, String middleName, String lastName, char gender, DateTime birthdate, String address){
        
        this.firstName = firstName;
        this.middleName = middleName;
        this.lastName = lastName;
        this.age = calculateAge(birthdate);
        this.gender = gender;
        this.birthdate = birthdate;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMiddleName() {
        return middleName;
    }

    public void setMiddleName(String middleName) {
        this.middleName = middleName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public char getGender() {
        return gender;
    }

    public void setGender(char gender) {
        this.gender = gender;
    }

    public DateTime getBirthdate() {
        return birthdate;
    }

    public void setBirthdate(DateTime birthdate) {
        this.birthdate = birthdate;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }  

    @Override
    public String toString() {
        return String.format("%-40s " + gender + "\t " + birthdate.toString("MMMM dd, yyyy") + " (" + age + ")", lastName.toUpperCase() + ", " + firstName + " " + middleName);
    }
    
    private int calculateAge(DateTime birthdate){
        return new Period(birthdate.getMillis(), new DateTime().getMillis(), PeriodType.years()).getYears();
    }
}
